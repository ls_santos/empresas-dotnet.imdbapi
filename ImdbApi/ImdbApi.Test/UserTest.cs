using ImdbApi.Api.Models.Entities;
using ImdbApi.Api.Models.Requests;
using ImdbApi.Api.Services.Repositories.UserRepository;
using ImdbApi.Api.Services.UserService;
using NSubstitute;
using NSubstitute.ReturnsExtensions;
using System.Threading.Tasks;
using Xunit;

namespace ImdbApi.Test
{
    public class UserTest
    {
        [Fact]
        public async Task ShouldAddAdminUser()
        {
            var userRepository = Substitute.For<IUserRepository>();
            var userService = new UserService(userRepository);

            userRepository.FindByEmail(Arg.Any<string>()).ReturnsNull();

            var model = new UserRequestModel
            {
                Name = "Test",
                Email = "test@email.com",
                Password = "12345678"
            };

            var result = await userService.AddUser(model, true);

            Assert.True(result.Success);
        }

        [Fact]
        public async Task ShouldNotAddAdminUserIfAlreadyExist()
        {
            var userRepository = Substitute.For<IUserRepository>();
            var userService = new UserService(userRepository);

            userRepository.FindByEmail(Arg.Any<string>()).Returns(new User());

            var model = new UserRequestModel
            {
                Name = "Test",
                Email = "test@email.com",
                Password = "12345678"
            };

            var result = await userService.AddUser(model);

            Assert.False(result.Success);
            Assert.Equal("userAlreadyExist", result.ErrorMessage);
        }
    }
}
