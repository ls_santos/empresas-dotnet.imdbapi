﻿namespace ImdbApi.Api.Models.Requests
{
    public class RateMovieRequestModel
    {
        public int MovieId { get; set; }
        public double Score { get; set; }
    }
}
