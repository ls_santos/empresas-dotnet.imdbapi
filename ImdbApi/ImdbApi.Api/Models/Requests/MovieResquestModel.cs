﻿using ImdbApi.Api.Models.Enums;
using System.Collections.Generic;

namespace ImdbApi.Api.Models.Requests
{
    public class MovieResquestModel
    {
        public string Title { get; set; }
        public string Director { get; set; }
        public MovieGenre Genre { get; set; }
        public List<string> Cast { get; set; }
    }
}
