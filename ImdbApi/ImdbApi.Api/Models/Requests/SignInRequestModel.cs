﻿namespace ImdbApi.Api.Models.Requests
{
    public class SignInRequestModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
