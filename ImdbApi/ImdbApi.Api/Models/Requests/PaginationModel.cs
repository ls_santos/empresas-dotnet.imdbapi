﻿namespace ImdbApi.Api.Models.Requests
{
    public class PaginationModel
    {
        public int Index { get; set; } = 0;
        public int Length { get; set; } = 30;
    }
}
