﻿namespace ImdbApi.Api.Models.Requests
{
    public class UpdateUserRequestModel
    {
        public string Name { get; set; }
        public string Password { get; set; }
    }
}
