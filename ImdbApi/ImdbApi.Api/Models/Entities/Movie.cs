﻿using ImdbApi.Api.Models.Enums;
using System.Collections.Generic;

namespace ImdbApi.Api.Models.Entities
{
    public class Movie
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Director { get; set; }
        public MovieGenre Genre { get; set; }
        public List<Cast> Cast { get; set; }
        public List<Rating> Ratings { get; set; }

        public Movie() { }

        public Movie(string title, string director, MovieGenre genre, List<Cast> cast)
        {
            Title = title;
            Director = director;
            Genre = genre;
            Cast = cast;
        }
    }
}
