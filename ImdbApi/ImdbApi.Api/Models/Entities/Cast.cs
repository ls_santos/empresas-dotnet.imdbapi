﻿namespace ImdbApi.Api.Models.Entities
{
    public class Cast
    {
        public int MovieId { get; set; }
        public string Name { get; set; }
        public Movie Movie { get; set; }

        public Cast() { }

        public Cast(string name)
        {
            Name = name;
        }
    }
}
