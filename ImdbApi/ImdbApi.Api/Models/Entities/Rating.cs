﻿namespace ImdbApi.Api.Models.Entities
{
    public class Rating
    {
        public int UserId { get; set; }
        public int MovieId { get; set; }
        public double Score { get; set; }
        public User User { get; set; }
        public Movie Movie { get; set; }

        public Rating() { }

        public Rating(double score, int userId)
        {
            Score = score;
            UserId = userId;
        }
    }
}
