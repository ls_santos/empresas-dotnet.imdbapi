﻿using System;
using System.Collections.Generic;

namespace ImdbApi.Api.Models.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public DateTime RegisterDate { get; set; }
        public bool Active { get; set; }
        public bool Admin { get; set; }
        public List<Rating> Ratings { get; set; }

        public User() { }

        public User(string name, string email, string password, bool isAdmin)
        {
            Name = name;
            Email = email;
            Password = password;
            RegisterDate = DateTime.Now;
            Active = true;
            Admin = isAdmin;
        }
    }
}
