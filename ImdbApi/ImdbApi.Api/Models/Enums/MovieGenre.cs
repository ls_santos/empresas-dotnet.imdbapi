﻿namespace ImdbApi.Api.Models.Enums
{
    public enum MovieGenre
    {
        Action,
        Adventure,
        Drama,
        Comedy,
        Horror,
        Invalid
    }

    public static class MovieGenreExtension
    {
        public static string AsString(this MovieGenre genre)
        {
            switch (genre)
            {
                case MovieGenre.Action:
                    return "action";
                case MovieGenre.Adventure:
                    return "adventure";
                case MovieGenre.Drama:
                    return "drama";
                case MovieGenre.Comedy:
                    return "comedy";
                case MovieGenre.Horror:
                    return "horror";
                default:
                    return null;
            }
        }
    }
}
