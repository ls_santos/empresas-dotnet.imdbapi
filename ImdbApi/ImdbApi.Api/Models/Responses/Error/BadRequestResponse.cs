﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace ImdbApi.Api.Models.Responses.Error
{
    public class BadRequestResponse : IActionResult
    {
        public IEnumerable<string> Errors { get; set; }

        public BadRequestResponse(params string[] errors)
        {
            Errors = errors;
        }

        public async Task ExecuteResultAsync(ActionContext context)
        {
            context.HttpContext.Response.StatusCode = (int)HttpStatusCode.UnprocessableEntity;
            await new JsonResult(this).ExecuteResultAsync(context);
        }
    }
}
