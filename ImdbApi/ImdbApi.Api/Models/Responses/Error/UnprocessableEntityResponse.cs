﻿using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;

namespace ImdbApi.Api.Models.Responses.Error
{
    public class UnprocessableEntityResponse : IActionResult
    {
        public string Error { get; set; }

        public UnprocessableEntityResponse(string error)
        {
            Error = error;
        }

        public async Task ExecuteResultAsync(ActionContext context)
        {
            context.HttpContext.Response.StatusCode = (int)HttpStatusCode.UnprocessableEntity;
            await new JsonResult(this).ExecuteResultAsync(context);
        }
    }
}
