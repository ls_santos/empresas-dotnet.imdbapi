﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ImdbApi.Api.Models.Responses.Success
{
    public class SignInResponse : IActionResult
    {
        public string Token { get; set; }

        public SignInResponse() { }

        public SignInResponse(string token)
        {
            Token = token;
        }

        public async Task ExecuteResultAsync(ActionContext context)
        {
            await new JsonResult(this).ExecuteResultAsync(context);
        }
    }
}
