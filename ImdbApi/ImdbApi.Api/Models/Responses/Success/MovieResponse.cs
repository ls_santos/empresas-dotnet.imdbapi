﻿using ImdbApi.Api.Models.Entities;
using ImdbApi.Api.Models.Enums;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ImdbApi.Api.Models.Responses.Success
{
    public class MovieResponse : IActionResult
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Director { get; set; }
        public string Genre { get; set; }
        public List<string> Cast { get; set; }

        public MovieResponse(Movie movie)
        {
            Id = movie.Id;
            Title = movie.Title;
            Director = movie.Director;
            Genre = movie.Genre.AsString();
            Cast = movie.Cast.Select(cast => cast.Name).ToList();
        }

        public async Task ExecuteResultAsync(ActionContext context)
        {
            context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Created;
            await new JsonResult(this).ExecuteResultAsync(context);
        }
    }
}
