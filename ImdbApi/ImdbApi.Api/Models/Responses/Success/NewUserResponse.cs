﻿using ImdbApi.Api.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Threading.Tasks;

namespace ImdbApi.Api.Models.Responses.Success
{
    public class NewUserResponse : IActionResult
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public DateTime RegisterDate { get; set; }

        public NewUserResponse(User user)
        {
            Id = user.Id;
            Name = user.Name;
            Email = user.Email;
            RegisterDate = user.RegisterDate;
        }

        public async Task ExecuteResultAsync(ActionContext context)
        {
            context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Created;
            await new JsonResult(this).ExecuteResultAsync(context);
        }
    }
}
