﻿using ImdbApi.Api.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImdbApi.Api.Models.Responses.Success
{
    public class MoviesListResponse : IActionResult
    {
        public List<MovieDetailResponse> Movies { get; set; }
        public int Count { get; set; }

        public MoviesListResponse(List<Movie> movies, int count)
        {
            Movies = movies.Select(movie => new MovieDetailResponse(movie))
                .OrderByDescending(movie => movie.AvarageScore)
                .ThenBy(movie => movie.Title)
                .ToList();
            Count = count;
        }

        public async Task ExecuteResultAsync(ActionContext context)
        {
            await new JsonResult(this).ExecuteResultAsync(context);
        }
    }
}
