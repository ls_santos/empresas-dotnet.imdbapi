﻿using ImdbApi.Api.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImdbApi.Api.Models.Responses.Success
{
    public class MovieDetailResponse : IActionResult
    {
        public string Title { get; set; }
        public string Director { get; set; }
        public double? AvarageScore { get; set; }
        public List<string> Cast { get; set; }

        public MovieDetailResponse(Movie movie)
        {
            Title = movie.Title;
            Director = movie.Director;
            AvarageScore = movie.Ratings != null && movie.Ratings.Any() 
                ? movie.Ratings.Average(rating => rating.Score) 
                : (double?)null;
            Cast = movie.Cast.Select(cast => cast.Name).ToList();
        }

        public async Task ExecuteResultAsync(ActionContext context)
        {
            await new JsonResult(this).ExecuteResultAsync(context);
        }
    }
}
