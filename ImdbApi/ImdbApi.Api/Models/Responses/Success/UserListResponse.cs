﻿using ImdbApi.Api.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImdbApi.Api.Models.Responses.Success
{
    public class UserListResponse : IActionResult
    {
        public List<NewUserResponse> Users { get; set; }
        public int Count { get; set; }

        public UserListResponse(List<User> users, int count)
        {
            Users = users.Select(user => new NewUserResponse(user))
                .OrderBy(user => user.Name)
                .ToList();
            Count = count;
        }

        public async Task ExecuteResultAsync(ActionContext context)
        {
            await new JsonResult(this).ExecuteResultAsync(context);
        }
    }
}
