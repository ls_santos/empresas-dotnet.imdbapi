﻿using ImdbApi.Api.Models.Entities;

namespace ImdbApi.Api.Services.JwtService
{
    public interface IJwtService
    {
        public string GenerateToken(User user);
    }
}
