﻿using ImdbApi.Api.Models.Entities;
using ImdbApi.Api.Models.Requests;
using ImdbApi.Api.Services.MovieService.ResponseModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImdbApi.Api.Services.MovieService
{
    public interface IMovieService
    {
        public Task<MovieResponse> AddMovie(MovieResquestModel model);
        public Task<MovieResponse> RateMovie(RateMovieRequestModel model, int userId);
        public Task<(List<Movie> movies, int count)> ListMovies(MoviesFilter filter);
        public Task<MovieResponse> GetMovie(int movieId);
    }
}
