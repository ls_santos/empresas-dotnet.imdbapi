﻿using ImdbApi.Api.Models.Entities;
using ImdbApi.Api.Models.Requests;
using ImdbApi.Api.Services.MovieService.ResponseModels;
using ImdbApi.Api.Services.Repositories.MovieRepository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImdbApi.Api.Services.MovieService
{
    public class MovieService : IMovieService
    {
        private readonly IMovieRepository _movieRepository;

        public MovieService(IMovieRepository movieRepository)
        {
            _movieRepository = movieRepository;
        }

        public async Task<MovieResponse> AddMovie(MovieResquestModel model)
        {
            var movie = await _movieRepository.FindByTitleAndDirector(model.Title, model.Director);

            if (movie != null)
            {
                return new MovieResponse
                {
                    Success = false,
                    ErrorMessage = "movieAlreadyExists"
                };
            }

            var cast = new List<Cast>();
            foreach ( var name in model.Cast)
            {
                cast.Add(new Cast(name));
            }

            var newMovie = new Movie(title: model.Title, director: model.Director, genre: model.Genre, cast: cast);

            await _movieRepository.AddMovie(newMovie);

            return new MovieResponse
            {
                Success = true,
                Movie = newMovie
            };
        }

        public async Task<MovieResponse> RateMovie(RateMovieRequestModel model, int userId)
        {
            var movie = await _movieRepository.FindById(model.MovieId);

            if (movie == null)
            {
                return new MovieResponse
                {
                    Success = false,
                    ErrorMessage = "movieNotFound"
                };
            }

            if (movie.Ratings == null)
            {
                movie.Ratings = new List<Rating>();
            }

            movie.Ratings.Add(new Rating(score: model.Score, userId: userId));

            await _movieRepository.UpdateMovie(movie);

            return new MovieResponse
            {
                Success = true
            };
        }

        public async Task<(List<Movie> movies, int count)> ListMovies(MoviesFilter filter)
        {
            return await _movieRepository.ListMovies(filter);
        }

        public async Task<MovieResponse> GetMovie(int movieId)
        {
            var movie = await _movieRepository.FindById(movieId);

            if (movie == null)
            {
                return new MovieResponse
                {
                    Success = false,
                    ErrorMessage = "movieNotFound"
                };
            }

            return new MovieResponse
            {
                Success = true,
                Movie = movie
            };
        }
    }
}
