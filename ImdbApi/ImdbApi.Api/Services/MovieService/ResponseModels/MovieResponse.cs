﻿using ImdbApi.Api.Models.Entities;

namespace ImdbApi.Api.Services.MovieService.ResponseModels
{
    public class MovieResponse
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
        public Movie Movie { get; set; }
    }
}
