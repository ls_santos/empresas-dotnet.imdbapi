﻿using ImdbApi.Api.Models.Entities;
using ImdbApi.Api.Models.Requests;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImdbApi.Api.Services.Repositories.MovieRepository
{
    public interface IMovieRepository
    {
        public Task AddMovie(Movie movie);
        public Task UpdateMovie(Movie movie);
        public Task<Movie> FindByTitleAndDirector(string title, string director);
        public Task<Movie> FindById(int movieId);
        public Task<(List<Movie> movies, int count)> ListMovies(MoviesFilter filter);
    }
}
