﻿using ImdbApi.Api.Infra;
using ImdbApi.Api.Models.Entities;
using ImdbApi.Api.Models.Requests;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImdbApi.Api.Services.Repositories.MovieRepository
{
    public class MovieRepository : IMovieRepository
    {
        private readonly ApiDbContext _dbContext;

        public MovieRepository(ApiDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task AddMovie(Movie movie)
        {
            await _dbContext.Movies.AddAsync(movie);

            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateMovie(Movie movie)
        {
            _dbContext.Movies.Update(movie);

            await _dbContext.SaveChangesAsync();
        }

        public async Task<Movie> FindByTitleAndDirector(string title, string director)
        {
            return await _dbContext.Movies.SingleOrDefaultAsync(movie => movie.Title.Equals(title) && movie.Director.Equals(director));
        }

        public async Task<Movie> FindById(int movieId)
        {
            return await _dbContext.Movies
                .Include(movie => movie.Cast)
                .Include(movie => movie.Ratings)
                .SingleOrDefaultAsync(movie => movie.Id.Equals(movieId));
        }

        public async Task<(List<Movie> movies, int count)> ListMovies(MoviesFilter filter)
        {
            var query = _dbContext.Movies.AsQueryable();

            if (filter.Title != null)
            {
                query = query.Where(movie => movie.Title.Contains(filter.Title));
            }

            if (filter.Director != null)
            {
                query = query.Where(movie => movie.Director.Contains(filter.Director));
            }

            if (filter.Genre != null)
            {
                query = query.Where(movie => movie.Genre.Equals(filter.Genre));
            }

            if (filter.Cast != null && filter.Cast.Any())
            {
                query = query.Where(movie => movie.Cast.Any(cast => filter.Cast.Contains(cast.Name)));
            }

            var count = await query.CountAsync();

            var movies = await query
                .Include(movie => movie.Cast)
                .Include(movie => movie.Ratings)
                .Skip(filter.Index)
                .Take(filter.Length)
                .OrderBy(movie => movie.Ratings.Max(rating => rating.Score)).ThenBy(movie => movie.Title)
                .ToListAsync();

            return (movies, count);
        }
    }
}
