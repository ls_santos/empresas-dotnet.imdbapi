﻿using ImdbApi.Api.Models.Entities;
using ImdbApi.Api.Models.Requests;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImdbApi.Api.Services.Repositories.UserRepository
{
    public interface IUserRepository
    {
        public Task<User> FindById(int userId);
        public Task<User> FindByEmail(string email);
        public Task AddUser(User user);
        public Task UpdateUser(User user);
        public Task<(List<User> users, int count)> GetNotAdminUsers(PaginationModel model);
    }
}
