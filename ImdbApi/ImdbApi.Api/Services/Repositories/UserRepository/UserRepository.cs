﻿using ImdbApi.Api.Infra;
using ImdbApi.Api.Models.Entities;
using ImdbApi.Api.Models.Requests;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImdbApi.Api.Services.Repositories.UserRepository
{
    public class UserRepository : IUserRepository
    {
        private readonly ApiDbContext _dbContext;

        public UserRepository(ApiDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<User> FindById(int userId)
        {
            return await _dbContext.Users.SingleOrDefaultAsync(user => user.Id.Equals(userId));
        }

        public async Task<User> FindByEmail(string email)
        {
            return await _dbContext.Users.SingleOrDefaultAsync(user => user.Email.Equals(email));
        }

        public async Task AddUser(User user)
        {
            await _dbContext.Users.AddAsync(user);

            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateUser(User user)
        {
            _dbContext.Users.Update(user);

            await _dbContext.SaveChangesAsync();
        }

        public async Task<(List<User> users, int count)> GetNotAdminUsers(PaginationModel model)
        {
            var query = _dbContext.Users
                .Where(user => !user.Admin);

            var count = await query.CountAsync();
            
            var users = await query
                .Skip(model.Index)
                .Take(model.Length)
                .ToListAsync();

            return (users, count);
        }
    }
}
