﻿namespace ImdbApi.Api.Services.AuthenticationService.ResponseModels
{
    public class SignInResponse
    {
        public bool Success { get; set; }
        public string ErrorType { get; set; }
        public string Token { get; set; }
    }
}
