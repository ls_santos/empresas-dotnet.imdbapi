﻿using ImdbApi.Api.Infra.Extensions;
using ImdbApi.Api.Services.AuthenticationService.ResponseModels;
using ImdbApi.Api.Services.JwtService;
using ImdbApi.Api.Services.Repositories.UserRepository;
using System.Threading.Tasks;

namespace ImdbApi.Api.Services.AuthenticationService
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IUserRepository _userRepository;
        private readonly IJwtService _jwtService;

        public AuthenticationService(IUserRepository userRepository, IJwtService jwtService)
        {
            _userRepository = userRepository;
            _jwtService = jwtService;
        }

        public async Task<SignInResponse> SignIn(string email, string password)
        {
            var user = await _userRepository.FindByEmail(email);

            if (user == null)
            {
                return new SignInResponse
                {
                    Success = false,
                    ErrorType = "userNotFound"
                };
            }

            var md5Password = password.GetMd5Hash();
            if (user.Password.Equals(md5Password))
            {
                var token = _jwtService.GenerateToken(user);

                return new SignInResponse
                {
                    Success = true,
                    Token = token
                };
            }
            else
            {
                return new SignInResponse
                {
                    Success = false,
                    ErrorType = "invalidPassword"
                };
            }
        }
    }
}
