﻿using ImdbApi.Api.Services.AuthenticationService.ResponseModels;
using System.Threading.Tasks;

namespace ImdbApi.Api.Services.AuthenticationService
{
    public interface IAuthenticationService
    {
        public Task<SignInResponse> SignIn(string email, string password);
    }
}
