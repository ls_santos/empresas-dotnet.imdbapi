﻿using ImdbApi.Api.Models.Entities;
using ImdbApi.Api.Models.Requests;
using ImdbApi.Api.Services.UserService.ResponseModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImdbApi.Api.Services.UserService
{
    public interface IUserService
    {
        public Task<NewUserResponse> AddUser(UserRequestModel model, bool isAdmin = false);
        public Task<UpdateUserResponse> UpdateUser(int userId, UpdateUserRequestModel model);
        public Task<(List<User> users, int count)> GetNotAdminUsers(PaginationModel model);
        public Task<UpdateUserResponse> Deactivate(int userId);
    }
}
