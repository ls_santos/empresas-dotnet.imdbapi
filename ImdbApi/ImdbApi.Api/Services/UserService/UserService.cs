﻿using ImdbApi.Api.Infra.Extensions;
using ImdbApi.Api.Models.Entities;
using ImdbApi.Api.Models.Requests;
using ImdbApi.Api.Services.Repositories.UserRepository;
using ImdbApi.Api.Services.UserService.ResponseModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImdbApi.Api.Services.UserService
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<NewUserResponse> AddUser(UserRequestModel model, bool isAdmin = false)
        {
            var user = await _userRepository.FindByEmail(model.Email.Trim());

            if (user != null)
            {
                return new NewUserResponse
                {
                    Success = false,
                    ErrorMessage = "userAlreadyExist"
                };
            }

            var md5Password = model.Password.GetMd5Hash();

            var newUser = new User(name: model.Name, email: model.Email, password: md5Password, isAdmin: isAdmin);

            await _userRepository.AddUser(newUser);

            return new NewUserResponse
            {
                Success = true,
                User = newUser
            };
        }

        public async Task<UpdateUserResponse> UpdateUser(int userId, UpdateUserRequestModel model)
        {
            var user = await _userRepository.FindById(userId);

            if (user == null)
            {
                return new UpdateUserResponse
                {
                    Success = false,
                    ErrorMessage = "userNotFound"
                };
            }

            if (model.Name != null)
            {
                user.Name = model.Name;
            }

            if (model.Password != null)
            {
                user.Password = model.Password.GetMd5Hash();
            }

            await _userRepository.UpdateUser(user);

            return new UpdateUserResponse
            {
                Success = true
            };
        }

        public async Task<(List<User> users, int count)> GetNotAdminUsers(PaginationModel model)
        {
            return await _userRepository.GetNotAdminUsers(model);
        }

        public async Task<UpdateUserResponse> Deactivate(int userId)
        {
            var user = await _userRepository.FindById(userId);

            if (user == null)
            {
                return new UpdateUserResponse
                {
                    Success = false,
                    ErrorMessage = "userNotFound"
                };
            }

            user.Active = false;

            await _userRepository.UpdateUser(user);

            return new UpdateUserResponse
            {
                Success = true
            };
        }
    }
}
