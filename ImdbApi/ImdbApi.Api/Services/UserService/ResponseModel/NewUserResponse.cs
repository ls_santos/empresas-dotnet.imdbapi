﻿using ImdbApi.Api.Models.Entities;

namespace ImdbApi.Api.Services.UserService.ResponseModel
{
    public class NewUserResponse
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
        public User User { get; set; }
    }
}
