﻿namespace ImdbApi.Api.Services.UserService.ResponseModel
{
    public class UpdateUserResponse
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }
}
