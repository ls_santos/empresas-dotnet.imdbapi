﻿using ImdbApi.Api.Infra.Extensions;
using ImdbApi.Api.Models.Requests;
using ImdbApi.Api.Models.Responses.Error;
using ImdbApi.Api.Models.Responses.Success;
using ImdbApi.Api.Services.MovieService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ImdbApi.Api.Controllers
{

    [ApiController]
    [Route("api/v1/movie")]
    public class MovieController : ControllerBase
    {
        private readonly IMovieService _movieService;

        public MovieController(IMovieService movieService)
        {
            _movieService = movieService;
        }

        [HttpPost(), Authorize(Policy = "Admin")]
        public async Task<IActionResult> AddMovie([FromBody] MovieResquestModel model)
        {
            var result = await _movieService.AddMovie(model);

            if (!result.Success)
            {
                return new UnprocessableEntityResponse(result.ErrorMessage);
            }

            return new MovieResponse(result.Movie);
        }

        [HttpPost("rating"), Authorize(Policy = "User")]
        public async Task<IActionResult> RateMovie([FromBody] RateMovieRequestModel model)
        {
            if (model.Score > 4)
            {
                return new BadRequestResponse("score must be lower than 4");
            }

            var userId = User.Claims.UserId();

            var result = await _movieService.RateMovie(model, userId);

            if (!result.Success)
            {
                return new UnprocessableEntityResponse(result.ErrorMessage);
            }

            return NoContent();
        }

        [HttpPost("all")]
        public async Task<IActionResult> ListMovies([FromBody] MoviesFilter filter)
        {
            var (movies, count) = await _movieService.ListMovies(filter);

            return new MoviesListResponse(movies, count);
        }

        [HttpGet("detail/{movieId}")]
        public async Task<IActionResult> GetMovieDetail([FromRoute] int movieId)
        {
            var result = await _movieService.GetMovie(movieId);

            if (!result.Success)
            {
                return new UnprocessableEntityResponse(result.ErrorMessage);
            }

            return new MovieDetailResponse(result.Movie);
        }
    }
}
