﻿using ImdbApi.Api.Models.Requests;
using ImdbApi.Api.Models.Responses.Success;
using ImdbApi.Api.Services.AuthenticationService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ImdbApi.Api.Controllers
{
    [ApiController]
    [Route("api/v1/auth")]
    public class AuthenticationController : ControllerBase
    {
        private readonly IAuthenticationService _authenticationService;

        public AuthenticationController(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        [HttpPost("signin"), AllowAnonymous]
        public async Task<IActionResult> SignIn([FromBody] SignInRequestModel model)
        {
            var authenticationResult = await _authenticationService.SignIn(model.Email, model.Password);


            return new SignInResponse(authenticationResult.Token);
        }
    }
}
