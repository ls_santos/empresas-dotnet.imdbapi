﻿using ImdbApi.Api.Models.Requests;
using ImdbApi.Api.Models.Responses.Error;
using ImdbApi.Api.Models.Responses.Success;
using ImdbApi.Api.Services.UserService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ImdbApi.Api.Controllers
{
    [ApiController]
    [Route("api/v1/user")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost(), AllowAnonymous]
        public async Task<IActionResult> NewUser([FromBody] UserRequestModel model)
        {
            var result = await _userService.AddUser(model);

            if (!result.Success)
            {
                return new UnprocessableEntityResponse(result.ErrorMessage);
            }

            return new NewUserResponse(result.User);
        }

        [HttpPut("{userId}")]
        public async Task<IActionResult> UpdateUser([FromRoute] int userId, UpdateUserRequestModel model)
        {
            var result = await _userService.UpdateUser(userId, model);

            if (!result.Success)
            {
                return new UnprocessableEntityResponse(result.ErrorMessage);
            }

            return NoContent();
        }

        [HttpPut("/deactivate/{userId}")]
        public async Task<IActionResult> DeactivateUser([FromRoute] int userId)
        {
            var result = await _userService.Deactivate(userId);

            if (!result.Success)
            {
                return new UnprocessableEntityResponse(result.ErrorMessage);
            }

            return NoContent();
        }
    }
}
