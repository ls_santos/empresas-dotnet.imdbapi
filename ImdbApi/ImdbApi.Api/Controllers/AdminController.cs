﻿using ImdbApi.Api.Models.Requests;
using ImdbApi.Api.Models.Responses.Success;
using ImdbApi.Api.Services.UserService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ImdbApi.Api.Controllers
{
    [ApiController]
    [Route("api/v1/admin")]
    public class AdminController : ControllerBase
    {
        private readonly IUserService _userService;

        public AdminController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost(), Authorize(Policy = "Admin")]
        public async Task<IActionResult> Add([FromBody] UserRequestModel model)
        {
            var result = await _userService.AddUser(model, true);

            if (!result.Success)
            {
                return UnprocessableEntity(result.ErrorMessage);
            }

            return new NewUserResponse(result.User);
        }

        [HttpPut("{userId:int}"), Authorize(Policy = "Admin")]
        public async Task<IActionResult> Update([FromRoute] int userId, [FromBody] UpdateUserRequestModel model)
        {
            var result = await _userService.UpdateUser(userId, model);

            if (!result.Success)
            {
                return UnprocessableEntity(result.ErrorMessage);
            }

            return NoContent();
        }

        [HttpGet(), Authorize(Policy = "Admin")]
        public async Task<IActionResult> ListNotAdminUsers([FromQuery] PaginationModel model)
        {
            var (users, count) = await _userService.GetNotAdminUsers(model);

            return new UserListResponse(users, count);
        }

        [HttpPut("/deactivate/{adminId:int}"), Authorize(Policy = "Admin")]
        public async Task<IActionResult> Deactivate([FromRoute] int adminId)
        {
            var result = await _userService.Deactivate(adminId);

            if (!result.Success)
            {
                return UnprocessableEntity(result.ErrorMessage);
            }

            return NoContent();
        }
    }
}
