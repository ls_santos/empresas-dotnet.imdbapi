﻿using ImdbApi.Api.Models.Enums;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System;
using System.Linq.Expressions;

namespace ImdbApi.Api.Infra.ValueConverters
{
    public class MovieGenreConverter : ValueConverter<MovieGenre, string>
    {
        public static MovieGenreConverter Instance { get; } = new MovieGenreConverter();

        public MovieGenreConverter() : base (ConvertToString, ConvertToEnum, null) { }

        private static Expression<Func<MovieGenre, string>> ConvertToString =>
            enumValue => EnumToString(enumValue);

        private static Expression<Func<string, MovieGenre>> ConvertToEnum =>
            stringValue => StringToEnum(stringValue);

        private static string EnumToString(MovieGenre genre)
        {
            return genre switch
            {
                MovieGenre.Action => MovieGenre.Action.AsString(),
                MovieGenre.Adventure => MovieGenre.Adventure.AsString(),
                MovieGenre.Drama => MovieGenre.Drama.AsString(),
                MovieGenre.Comedy => MovieGenre.Comedy.AsString(),
                MovieGenre.Horror => MovieGenre.Horror.AsString(),
                _ => null,
            };
        }

        private static MovieGenre StringToEnum(string stringValue)
        {
            return stringValue switch
            {
                "action" => MovieGenre.Action,
                "adventure" => MovieGenre.Adventure,
                "drama" => MovieGenre.Drama,
                "comedy" => MovieGenre.Drama,
                "horror" => MovieGenre.Horror,
                _ => MovieGenre.Invalid,
            };
        }
    }
}
