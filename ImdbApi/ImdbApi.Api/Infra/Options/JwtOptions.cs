﻿namespace ImdbApi.Api.Infra.Options
{
    public class JwtOptions
    {
        public string Secret { get; set; }
        public int ExpirationInMinutes { get; set; }
    }
}
