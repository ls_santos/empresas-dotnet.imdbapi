﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace ImdbApi.Api.Infra.Extensions
{
    public static class ClaimsExtension
    {
        public static int UserId(this IEnumerable<Claim> claims)
        {
            var claim = claims.FirstOrDefault(c => c.Type == ClaimTypes.Hash);

            int.TryParse(claim?.Value, out int ownerId);

            return ownerId;
        }
    }
}
