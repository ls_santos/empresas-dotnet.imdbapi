﻿using System.Security.Cryptography;
using System.Text;

namespace ImdbApi.Api.Infra.Extensions
{
    public static class Md5Extension
    {
        public static string GetMd5Hash(this string password)
        {
            using MD5 md5Hash = MD5.Create();
            var data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(password));

            var sBuilder = new StringBuilder();

            foreach (var byteData in data)
            {
                sBuilder.Append(byteData.ToString("x2"));
            }

            return sBuilder.ToString();
        }
    }
}
