﻿using ImdbApi.Api.Infra.Mappings;
using ImdbApi.Api.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace ImdbApi.Api.Infra
{
    public class ApiDbContext : DbContext
    {
        public ApiDbContext(DbContextOptions options) : base(options) { }

        public DbSet<User> Users { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Cast> Casts { get; set; }
        public DbSet<Rating> Ratings { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().Map();
            modelBuilder.Entity<Movie>().Map();
            modelBuilder.Entity<Cast>().Map();
            modelBuilder.Entity<Rating>().Map();
        }
    }
}
