﻿using ImdbApi.Api.Infra.ValueConverters;
using ImdbApi.Api.Models.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ImdbApi.Api.Infra.Mappings
{
    public static class MovieMap
    {
        public static void Map(this EntityTypeBuilder<Movie> entity)
        {
            entity.HasKey(p => p.Id);

            entity.Property(p => p.Title).IsRequired();
            entity.Property(p => p.Director).IsRequired();
            entity.Property(p => p.Genre).HasConversion(MovieGenreConverter.Instance);
        }
    }
}
