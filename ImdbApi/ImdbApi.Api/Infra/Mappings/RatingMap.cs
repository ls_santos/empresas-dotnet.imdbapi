﻿using ImdbApi.Api.Models.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ImdbApi.Api.Infra.Mappings
{
    public static class RatingMap
    {
        public static void Map(this EntityTypeBuilder<Rating> entity)
        {
            entity.HasKey(p => new { p.UserId, p.MovieId });

            entity.Property(p => p.UserId);
            entity.Property(p => p.MovieId);
            entity.Property(p => p.Score);

            entity.HasOne(rating => rating.Movie).WithMany(movie => movie.Ratings).HasForeignKey(rating => rating.MovieId);
            entity.HasOne(rating => rating.User).WithMany(user => user.Ratings).HasForeignKey(rating => rating.UserId);
        }
    }
}
