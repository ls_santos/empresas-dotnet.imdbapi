﻿using ImdbApi.Api.Models.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ImdbApi.Api.Infra.Mappings
{
    public static class CastMap
    {
        public static void Map(this EntityTypeBuilder<Cast> entity)
        {
            entity.HasKey(p => new { p.MovieId, p.Name });

            entity.Property(p => p.MovieId);
            entity.Property(p => p.Name);

            entity.HasOne(cast => cast.Movie).WithMany(movie => movie.Cast).HasForeignKey(cast => cast.MovieId);
        }
    }
}
