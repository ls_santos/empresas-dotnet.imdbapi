﻿using ImdbApi.Api.Models.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ImdbApi.Api.Infra.Mappings
{
    public static class UserMap
    {
        public static void Map(this EntityTypeBuilder<User> entity)
        {
            entity.HasKey(p => p.Id);

            entity.Property(p => p.Name).IsRequired();
            entity.Property(p => p.Email).IsRequired();
            entity.Property(p => p.Password).IsRequired();
            entity.Property(p => p.RegisterDate).IsRequired();
            entity.Property(p => p.Active).IsRequired();
            entity.Property(p => p.Admin).IsRequired();
        }
    }
}
